import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncomeComponent } from './income.component';
import { IncomeListComponent } from './income-list/income-list.component';
import { IncomeBarComponent } from './income-bar/income-bar.component';

@NgModule({
  declarations: [
    IncomeComponent,
    IncomeListComponent,
    IncomeBarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class IncomeModule { }
