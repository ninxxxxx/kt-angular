import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeBarComponent } from './income-bar.component';

describe('IncomeBarComponent', () => {
  let component: IncomeBarComponent;
  let fixture: ComponentFixture<IncomeBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
