import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { TodoModule } from './todo/todo.module';
import { ReminderComponent } from './reminder/reminder.component';


export const routes: Route[] = [
  {
    path: '',
    redirectTo: 'reminder',
    pathMatch: 'full',
  },
  {
    path: 'reminder',
    component: ReminderComponent
  },
  {
    path: 'todo',
    loadChildren: () => import('./todo/todo.module').then(m => m.TodoModule)
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ReminderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    TodoModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
