import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'amazing-todo';
  title2 = 'amazing-todo3';


  onValueChange(inputText: string) {
    console.log(inputText);
  }
}
