import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormGroupName, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';

export const compareDateValidator: ValidatorFn = (controls: FormGroup): ValidationErrors | null => {
  const fromDate = controls.get('from').value;
  const toDate = controls.get('to').value;
  const isFromMoreThanTo = fromDate > toDate;
  const error = isFromMoreThanTo ? { fromLessThanTo: true } : null;
  return error;
}

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  public text: string;
  public errorMsgs: string;
  public isError: boolean;
  public userProfileForm: FormGroup;


  constructor(private fb: FormBuilder) {
    this.text = '';
    this.errorMsgs = '';
  }



  ngOnInit() {
    const creditCard = this.createCreditCardGroup();
    this.userProfileForm = this.fb.group({
      firstname: ['ewe'],
      lastname: ['wewewewe'],
      date: this.fb.group({
        from: [, Validators.required],
        to: [, Validators.required],
      }, { validators: [compareDateValidator] }),
      creditCards: this.fb.array([creditCard], [Validators.required])
    });
  }

  onEnter(text: string) {
    console.log(text);
    this.isError = text === '';
    if (text) {
      this.isError = false;
      this.text = text;
    }
  }

  get creditCards() {
    const array = this.userProfileForm.get('creditCards') as FormArray;
    return array;
  }


  createCreditCardGroup(): FormGroup {
    const creditCardGroup = this.fb.group({
      name: ['', Validators.required],
      bank: ['', Validators.required],
    });


    return creditCardGroup;
  }

  addCreditCard() {
    const creditCardGroup: FormGroup = this.createCreditCardGroup();
    this.creditCards.push(creditCardGroup);
  }

  onSubmit(form: FormGroup) {
    console.log(form.valid);
    console.log(form.value);
  }

}
