import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TodoComponent,
  TodoBarComponent,
  TodoListComponent
} from './';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { TodoDetailComponent } from '../todo-detail/todo-detail.component';


export const routes: Route[] = [
  {
    path: '',
    component: TodoComponent,
    children: [
      {
        path: '',
        component: TodoListComponent
      },
      {
        path: ':id',
        component: TodoDetailComponent
      },
      {
        path: 'bar',
        component: TodoBarComponent
      }
    ]
  },
];

@NgModule({
  declarations: [
    TodoComponent,
    TodoBarComponent,
    TodoListComponent,
    TodoDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  exports: [
    RouterModule,
    TodoComponent,
  ]
})
export class TodoModule { }





//files:
// component => .ts
// service => service.ts
// module => module.ts
// test => .spec.ts
// routing => .routing.ts

// type:
// interface
export interface User {
  name: string
}
// class
export class User {
  constructor(public name: string) {
    this.name = name;
  }
  readName() {
    return this.name
  }
}
// dto: always be verb
// CreateEvent => create-event.dto.ts
// export interface CreateEvent {

// }
// UpdateEvent



//naming:
//sort and readable


// Module
// MessageModule != MsgModule

// component
// MessagePanel => 'msg-panel'
// MessagePanelNav => 'msg-panel-nav'
// MessagePanelNavButton => 'msg-panel-nav-button'
// MessageList
// MessageItem



// AModule
//  a.module.ts
//  /comp1
//    /comp1childA
//    /comp1childB
//  /comp2
//  /services
//  /interfaces, /models
//  /dto


// import {Comp1Component} from './comp1/comp1.component'
// import {Comp1Component} from './comp1/comp1childA/comp1-child-a.component'
// import {Comp1Component} from './comp1/comp1childB/comp1-child-b.component'


// NgModule({
//   declarations: [
// //  /comp1
// //    /comp1childA
// //    /comp1childB
// //  /comp2
// Comp2Component
//   ],
//   providers: []
// })
// export class AModule {

// }


