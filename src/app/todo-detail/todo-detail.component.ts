import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(res => {
      console.log(res);
    });

    this.route.queryParamMap.subscribe(params => {
      console.log(params.get('user'));
    });
  }

  ngOnInit() {
  }

}
